package com.researchecosystems.contentserviceapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.PostgreSQLContainerProvider;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractIntegrationTest {


    @Autowired
    protected TestRestTemplate testRestTemplate;

    @Container
    static final GenericContainer<?> postgreSQLContainer = (PostgreSQLContainer<?>) new PostgreSQLContainerProvider().newInstance()
            .withDatabaseName("primevideo")
            .withUsername("sa")
            .withPassword("sa");

}
