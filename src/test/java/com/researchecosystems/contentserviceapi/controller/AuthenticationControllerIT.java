package com.researchecosystems.contentserviceapi.controller;

import com.researchecosystems.contentserviceapi.entity.User;
import com.researchecosystems.contentserviceapi.entity.UserRole;
import com.researchecosystems.contentserviceapi.model.request.auth.LoginRequest;
import com.researchecosystems.contentserviceapi.model.request.auth.PasswordChangeRequest;
import com.researchecosystems.contentserviceapi.model.request.auth.register.RegisterRequest;
import com.researchecosystems.contentserviceapi.model.response.LoginResponse;
import com.researchecosystems.contentserviceapi.repository.UserRepository;
import com.researchecosystems.contentserviceapi.service.client.EmailClient;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.jdbc.Sql;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AuthenticationControllerIT extends AbstractIntegrationTest{
    @Autowired
    private AuthController authController;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmailClient emailClient;
    @Autowired
    private PasswordEncoder passwordEncoder;
    private final User testUser = new User("test user"," userson","tester@mail.com","$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW", UserRole.USER,null,null,null,null,false,null,null,null);
    // password is "123456"

    @Test
    @Order(1)
    void ContextLoads() {
        Assertions.assertThat(testRestTemplate).isNotNull();
        Assertions.assertThat(postgreSQLContainer).isNotNull();
        Assertions.assertThat(postgreSQLContainer.isRunning()).isTrue();
        Assertions.assertThat(authController).isNotNull();
        Assertions.assertThat(userRepository).isNotNull();
        Assertions.assertThat(passwordEncoder).isNotNull();
    }

    @Test
    @Order(2)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenAUserRegisters_thenTheUserIsCreated() {

        Assertions.assertThat(userRepository.findByEmail(testUser.getEmail())).isNull();

        RegisterRequest registerRequest = new RegisterRequest(testUser.getEmail(),"123456");
        HttpEntity<RegisterRequest> registerRequestHttpEntity = new HttpEntity<>(registerRequest);
        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/register",
                        HttpMethod.POST,
                        registerRequestHttpEntity,
                        String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        User testerAtDB = userRepository.findByEmail(testUser.getEmail());
        Assertions.assertThat(testerAtDB).isNotNull();
    }



    @Test
    @Order(3)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenUnverifiedUserLogins_thenForbidden() {
        String unverifiedUserMail = "unverifieduser@mail.com";
        String testerPassword = "unverifieduserpassword";

        User tester = userRepository.findByEmail(unverifiedUserMail);
        Assertions.assertThat(tester).isNotNull();
        Assertions.assertThat(tester.isVerified()).isFalse();

        LoginRequest  authenticationRequest = new LoginRequest(unverifiedUserMail, testerPassword);
        HttpEntity<LoginRequest> authenticationRequestHttpEntity = new HttpEntity<>(authenticationRequest);

        ResponseEntity<LoginResponse> response =
                testRestTemplate.exchange(
                        "/auth/login",
                        HttpMethod.POST,
                        authenticationRequestHttpEntity,
                        LoginResponse.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }


    @Test
    @Order(4)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenVerifiedUserLogins_thenGivesJwtToken() {
        String verifiedUserMail = "verifieduser@mail.com";
        String verifiedUserPassword = "verifieduserpassword";

        User verifiedUser = userRepository.findByEmail(verifiedUserMail);
        Assertions.assertThat(verifiedUser).isNotNull();
        Assertions.assertThat(verifiedUser.isVerified()).isTrue();

        LoginRequest authenticationRequest = new LoginRequest(verifiedUserMail, verifiedUserPassword);
        HttpEntity<LoginRequest> authenticationRequestHttpEntity = new HttpEntity<>(authenticationRequest);

        ResponseEntity<LoginResponse> response =
                testRestTemplate.exchange(
                        "/auth/login",
                        HttpMethod.POST,
                        authenticationRequestHttpEntity,
                        LoginResponse.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();

    }


    @Test
    @Order(5)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenUserLoginsWithWrongPassword_thenConflict() {

        String verifiedUserMail = "verifieduser@mail.com";
        String wrongVerifiedUserPassword = "wrongpassword";

        User verifiedUser = userRepository.findByEmail(verifiedUserMail);
        Assertions.assertThat(verifiedUser).isNotNull();
        Assertions.assertThat(verifiedUser.isVerified()).isTrue();

        LoginRequest authenticationRequest = new LoginRequest(verifiedUserMail, wrongVerifiedUserPassword);
        HttpEntity<LoginRequest> authenticationRequestHttpEntity = new HttpEntity<>(authenticationRequest);

        ResponseEntity<LoginResponse> response =
                testRestTemplate.exchange(
                        "/auth/login",
                        HttpMethod.POST,
                        authenticationRequestHttpEntity,
                        LoginResponse.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
    }



    @Test
    @Order(6)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenUserLoginsWithWrongEmail_thenNotFound() {
        String wrongMail = "wrongemail@mail.com";
        String wrongPassword = "wrongpassword";

        User wrongUser = userRepository.findByEmail(wrongMail);
        Assertions.assertThat(wrongUser).isNull();

        LoginRequest authenticationRequest = new LoginRequest(wrongMail, wrongPassword);
        HttpEntity<LoginRequest> authenticationRequestHttpEntity = new HttpEntity<>(authenticationRequest);

        ResponseEntity<LoginResponse> response =
                testRestTemplate.exchange(
                        "/auth/login",
                        HttpMethod.POST,
                        authenticationRequestHttpEntity,
                        LoginResponse.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @Order(7)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenUnverifiedUserRequestsForVerificationEmail_thenEmailSent() {
        String unverifiedUserMail = "unverifieduser@mail.com";

        User unverifiedUser = userRepository.findByEmail(unverifiedUserMail);
        Assertions.assertThat(unverifiedUser).isNotNull();
        Assertions.assertThat(unverifiedUser.isVerified()).isFalse();
        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/verify/"+unverifiedUserMail,
                        HttpMethod.POST,
                        null,
                        String.class);


        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
    }

    @Test
    @Order(8)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenVerifiedUserRequestsForVerificationEmail_thenForbidden() {
        String verifiedUserMail = "verifieduser@mail.com";
        User verifiedUser = userRepository.findByEmail(verifiedUserMail);

        Assertions.assertThat(verifiedUser).isNotNull();
        Assertions.assertThat(verifiedUser.isVerified()).isTrue();

        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/verify/" + verifiedUserMail,
                        HttpMethod.POST,
                        null,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    @Order(9)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenVerificationLinkClicked_thenUserVerified() {
        String verificationCodeUserMail = "verificationcodeuser@mail.com";
        User verificationCodeUser = userRepository.findByEmail(verificationCodeUserMail);

        Assertions.assertThat(verificationCodeUser).isNotNull();
        Assertions.assertThat(verificationCodeUser.isVerified()).isFalse();
        Assertions.assertThat(verificationCodeUser.getVerificationCode()).isNotNull();

        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/verify/" + verificationCodeUserMail + "?code=" + verificationCodeUser.getVerificationCode(),
                        HttpMethod.GET,
                        null,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        User updatedUser = userRepository.findByEmail(verificationCodeUserMail);
        Assertions.assertThat(updatedUser).isNotNull();
        Assertions.assertThat(updatedUser.isVerified()).isTrue();
    }

    @Test
    @Order(10)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenVerificationLinkClickedWithWrongToken_thenConflict() {
        String verificationCodeUserMail = "verificationcodeuser@mail.com";
        String wrongVerificationCode = "wrongverificationcode";
        User verificationCodeUser = userRepository.findByEmail(verificationCodeUserMail);

        Assertions.assertThat(verificationCodeUser).isNotNull();
        Assertions.assertThat(verificationCodeUser.isVerified()).isFalse();
        Assertions.assertThat(verificationCodeUser.getVerificationCode()).isNotNull();
        Assertions.assertThat(verificationCodeUser.getVerificationCode()).isNotEqualTo(wrongVerificationCode);

        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/verify/" + verificationCodeUserMail + "?code=" + wrongVerificationCode,
                        HttpMethod.GET,
                        null,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        User updatedUser = userRepository.findByEmail(verificationCodeUserMail);
        Assertions.assertThat(updatedUser).isNotNull();
        Assertions.assertThat(updatedUser.isVerified()).isFalse();
    }

    @Test
    @Order(11)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenVerificationLinkClickedWithExpiredToken_thenGone() {
        String expiredVerificationCodeUserMail = "expiredverificationuser@mail.com";
        User expiredVerificationCodeUser = userRepository.findByEmail(expiredVerificationCodeUserMail);

        Assertions.assertThat(expiredVerificationCodeUser).isNotNull();
        Assertions.assertThat(expiredVerificationCodeUser.isVerified()).isFalse();
        Assertions.assertThat(expiredVerificationCodeUser.getVerificationCode()).isNotNull();
        Assertions.assertThat(expiredVerificationCodeUser.getVerificationCodeExpirationDate()).isNotNull();

        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/verify/" + expiredVerificationCodeUserMail + "?code=" + expiredVerificationCodeUser.getVerificationCode(),
                        HttpMethod.GET,
                        null,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.GONE);
        User updatedUser = userRepository.findByEmail(expiredVerificationCodeUserMail);
        Assertions.assertThat(updatedUser).isNotNull();
        Assertions.assertThat(updatedUser.isVerified()).isFalse();
        Assertions.assertThat(updatedUser.getVerificationCode()).isNull();
        Assertions.assertThat(updatedUser.getVerificationCodeExpirationDate()).isNull();

    }

    @Test
    @Order(12)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenUserRequestsForPasswordResetEmail_thenLinkSent() {
        String userMail = "verifieduser@mail.com";
        User user = userRepository.findByEmail(userMail);

        Assertions.assertThat(user).isNotNull();
        Assertions.assertThat(user.isVerified()).isTrue();

        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/recover/" + userMail,
                        HttpMethod.POST,
                        null,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
    }

    @Test
    @Order(13)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenUserRequestsForPasswordResetEmailWithWrongEmail_thenForbidden() {
        String wrongMail = "wrong@mail.com";
        User user = userRepository.findByEmail(wrongMail);

        Assertions.assertThat(user).isNull();

        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/recover/" + wrongMail,
                        HttpMethod.POST,
                        null,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @Order(14)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenUserRequestsForPasswordResetEmailWithUnverifiedEmail_thenForbidden() {
        String unverifiedUserMail = "unverifieduser@mail.com";
        User unverifiedUser = userRepository.findByEmail(unverifiedUserMail);

        Assertions.assertThat(unverifiedUser).isNotNull();
        Assertions.assertThat(unverifiedUser.isVerified()).isFalse();

        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/recover/" + unverifiedUserMail,
                        HttpMethod.POST,
                        null,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);

    }

    @Test
    @Order(15)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenPasswordResetLinkClicked_thenPasswordReset() {
        String userMail = "verifieduser@mail.com";
        String userPassword = "verifieduserpassword";
        String newPassword = "newpassword";

        User user = userRepository.findByEmail(userMail);

        Assertions.assertThat(user).isNotNull();
        Assertions.assertThat(user.isVerified()).isTrue();

        Assertions.assertThat(passwordEncoder.matches(userPassword, user.getPasswordHash())).isTrue();

        PasswordChangeRequest recoverPasswordRequest = new PasswordChangeRequest(newPassword, newPassword);
        HttpEntity<PasswordChangeRequest> recoverPasswordRequestHttpEntity = new HttpEntity<>(recoverPasswordRequest);
        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/recover/" + userMail + "?code=" + user.getRecoveryCode(),
                        HttpMethod.PUT,
                        recoverPasswordRequestHttpEntity,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        User updatedUser = userRepository.findByEmail(userMail);
        Assertions.assertThat(updatedUser).isNotNull();
        Assertions.assertThat(passwordEncoder.matches(newPassword, updatedUser.getPasswordHash())).isTrue();
        Assertions.assertThat(updatedUser.getRecoveryCode()).isNull();
    }

    @Test
    @Order(16)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenPasswordResetLinkClickedWithWrongToken_thenForbidden() {
        String userMail = "verifieduser@mail.com";
        String userPassword = "verifieduserpassword";
        String newPassword = "newpassword";

        String wrongRecoveryCode = "wrongrecoverycode";

        User user = userRepository.findByEmail(userMail);

        Assertions.assertThat(user).isNotNull();
        Assertions.assertThat(user.isVerified()).isTrue();
        Assertions.assertThat(user.getRecoveryCode().equals(wrongRecoveryCode)).isFalse();

        Assertions.assertThat(passwordEncoder.matches(userPassword, user.getPasswordHash())).isTrue();

        PasswordChangeRequest recoverPasswordRequest = new PasswordChangeRequest(newPassword, newPassword);
        HttpEntity<PasswordChangeRequest> recoverPasswordRequestHttpEntity = new HttpEntity<>(recoverPasswordRequest);
        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/recover/" + userMail + "?code=" + wrongRecoveryCode,
                        HttpMethod.PUT,
                        recoverPasswordRequestHttpEntity,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        User userAfterProcess = userRepository.findByEmail(userMail);
        Assertions.assertThat(userAfterProcess).isNotNull();
        Assertions.assertThat(passwordEncoder.matches(userPassword, userAfterProcess.getPasswordHash())).isTrue();
    }

    @Test
    @Order(17)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenPasswordResetLinkClickedWithExpiredToken_thenGone() {
        String expiredRecoveryUserMail = "expiredrecoveryuser@mail.com";
        String userPassword = "expiredrecoveryuserpassword";
        String newPassword = "newpassword";

        User user = userRepository.findByEmail(expiredRecoveryUserMail);

        Assertions.assertThat(user).isNotNull();
        Assertions.assertThat(user.isVerified()).isTrue();
        Assertions.assertThat(user.getRecoveryCode()).isNotNull();

        Assertions.assertThat(passwordEncoder.matches(userPassword, user.getPasswordHash())).isTrue();

        PasswordChangeRequest recoverPasswordRequest = new PasswordChangeRequest(newPassword, newPassword);
        HttpEntity<PasswordChangeRequest> recoverPasswordRequestHttpEntity = new HttpEntity<>(recoverPasswordRequest);

        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/recover/" + expiredRecoveryUserMail + "?code=" + user.getRecoveryCode(),
                        HttpMethod.PUT,
                        recoverPasswordRequestHttpEntity,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.GONE);
        User userAfterProcess = userRepository.findByEmail(expiredRecoveryUserMail);
        Assertions.assertThat(userAfterProcess).isNotNull();
        Assertions.assertThat(passwordEncoder.matches(userPassword, userAfterProcess.getPasswordHash())).isTrue();
        Assertions.assertThat(userRepository.findByEmail(expiredRecoveryUserMail).getRecoveryCode()).isNull();
    }
    @Test
    @Order(18)
    @Sql(scripts = {"/truncateTables.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void whenPasswordResetLinkClickedWithWrongAgainPassword_thenConflict() {
        String userMail = "verifieduser@mail.com";
        String userPassword = "verifieduserpassword";
        String newPassword = "newpassword";
        String wrongNewPassword = "wrongnewpassword";

        User user = userRepository.findByEmail(userMail);

        Assertions.assertThat(user).isNotNull();
        Assertions.assertThat(user.isVerified()).isTrue();

        Assertions.assertThat(passwordEncoder.matches(userPassword, user.getPasswordHash())).isTrue();

        PasswordChangeRequest recoverPasswordRequest = new PasswordChangeRequest(newPassword, wrongNewPassword);
        HttpEntity<PasswordChangeRequest> recoverPasswordRequestHttpEntity = new HttpEntity<>(recoverPasswordRequest);
        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        "/auth/recover/" + userMail + "?code=" + user.getRecoveryCode(),
                        HttpMethod.PUT,
                        recoverPasswordRequestHttpEntity,
                        String.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        User updatedUser = userRepository.findByEmail(userMail);
        Assertions.assertThat(updatedUser).isNotNull();
        Assertions.assertThat(passwordEncoder.matches(userPassword, updatedUser.getPasswordHash())).isTrue();
    }
}
