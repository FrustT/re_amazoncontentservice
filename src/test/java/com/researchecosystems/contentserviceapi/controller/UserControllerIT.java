package com.researchecosystems.contentserviceapi.controller;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserControllerIT extends AbstractIntegrationTest {



    @Test
    @Order(1)
    void ContextLoads() {
        Assertions.assertThat(testRestTemplate).isNotNull();
        Assertions.assertThat(postgreSQLContainer).isNotNull();
        Assertions.assertThat(postgreSQLContainer.isRunning()).isTrue();
    }
}
