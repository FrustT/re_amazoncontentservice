INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('d89cad27-a1b1-45f3-8562-3638c31aac7f', now(), now(), 'unverifieduser@mail.com', 'Unverified User', '$2a$10$C8YzpP8pUtPOvEYQb/TaG.C9WEYd1iZjda3Snn5nM6el1jNme6TQm',
        null, null, 'Userson', 'USER', null, null, false);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('e086cde0-1b16-4e16-89b5-9243e26c99ae', now(), now(), 'verifieduser@mail.com', 'Verified User', '$2a$10$RpSlLwu8bmtUjPfXptSU3Ou3PbvoDDeh.wyk0AUgYpI4OqQRTCa8q','$2a$10$Alw4MSkOmOOU5EE/CEzeNu.dvNIPslqNmOZDvyHfRmm7YpRe3UqaK',
        CURRENT_TIMESTAMP + INTERVAL '1 DAY','Userson', 'USER', null, null, true);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('c93dc62d-7a05-43d6-a7d2-9f6775deecea', now(), now(), 'expiredrecoveryuser@mail.com', 'Expired recovery user', '$2a$10$mqlZ2cJlWReqOMel0wDF5ecplmU7WmLQNXkNwoOe8wDYxZCPmVVvS','$2a$10$z8vAy8298.M5bZJvCyuH6OxFkXmu/HIXVVqXfpmGR2FzR9Rvn9hs.',
        CURRENT_TIMESTAMP - INTERVAL '1 DAY','Userson', 'USER', null, null, true);


INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('49588591-cb7d-4854-a495-361127128049', now(), now(), 'verificationcodeuser@mail.com', 'User with  valid verification code', '$2a$10$XaxntZSAm1a6qxkw/ViD/eXqnSbAuDRfAWrMLWZxXrbh3FyFwUoB6',null,null,'Userson', 'USER', '$2a$10$mKPTcKA98jPRc9ey6fERGOX/Xo5Xh8Ko0j8VQmEe7GSPSfuktDox',CURRENT_TIMESTAMP + INTERVAL '1 DAY', false);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('05987b23-d3b4-4126-8ea0-4454d2ac86c2', now(), now(), 'expiredverificationuser@mail.com', 'user with  expired verification code', '$2a$10$sFr6067gKHzz1delJQLN8eaPinh3/sjS/j8jMKPPBGOBvhRxrCgXm',null,null,'Userson', 'USER','$2a$10$mKPTcKA98jPRc9ey6fERGOX/Xo5Xh8Ko0j8VQmEe7GSPSfuktDox',CURRENT_TIMESTAMP - INTERVAL '1 DAY', false);