-- Passwords are 123456
INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('user1', now(), now(), 'test1@test.com', 'John', '$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW',
        null, null, 'Doe', 'ADMIN', null, null, true);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('user2', now(), now(), 'test2@test.com', 'John', '$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW',
        null, null, 'Doe', 'GUEST', null, null, true);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('user3', now(), now(), 'test3@test.com', 'John', '$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW',
        null, null, 'Doe', 'USER', null, null, true);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('user4', now(), now(), 'test4@test.com', 'John', '$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW',
        null, null, 'Doe', 'USER', null, null, true);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('user5', now(), now(), 'test5@test.com', 'John', '$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW',
        null, null, 'Doe', 'USER', null, null, true);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('user6', now(), now(), 'test6@test.com', 'John', '$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW',
        null, null, 'Doe', 'USER', null, null, true);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('user7', now(), now(), 'test7@test.com', 'John', '$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW',
        null, null, 'Doe', 'USER', null, null, true);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('user8', now(), now(), 'test8@test.com', 'John', '$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW',
        null, null, 'Doe', 'USER', null, null, true);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('user9', now(), now(), 'test9@test.com', 'John', '$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW',
        null, null, 'Doe', 'USER', null, null, true);

INSERT INTO users (id, created, updated, email, name, password_hash, recovery_code, recovery_code_expiration_date,
                   surname, role, verification_code, verification_code_expiration_date, is_verified)
VALUES ('user10', now(), now(), 'test10@test.com', 'John', '$2a$10$aqvJCH43/2Nt1JmoT5nn5OrnFt05VtjE87lI/xw7mnoSKO.NejYfW',
        null, null, 'Doe', 'USER', null, null, true);

insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category1',now(),now(),'action',true,null);
insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category2',now(),now(),'adventure',false,'category1');
insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category3',now(),now(),'science fiction',false,'category1');
insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category4',now(),now(),'thriller',false,'category1');
insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category5',now(),now(),'drama',true,null);
insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category6',now(),now(),'romance',true,'category5');
insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category7',now(),now(),'historical',true,'category5');
insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category8',now(),now(),'animation',false,'category1');
insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category9',now(),now(),'documentary',false,'category7');
insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category10',now(),now(),'comedy',true,'category5');
insert into categories (id,created,updated,name,is_root,parent_category_id)   values ('category11',now(),now(),'mystery',true,'category2');

insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content0', now(), now(), 'Neighbor, The', 'category8', '6/13/1928', 'Isadora Jenner', 125, 6.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content1', now(), now(), 'Blackout', 'category10', '7/5/1951', 'Rodolph Sadlier', 92, 8.9);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content2', now(), now(), 'Less is More (Menos es más)', 'category1', '3/31/1950', 'Jervis Teggart', 155, 1.4);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content3', now(), now(), 'Alabama''s Ghost', 'category8', '7/16/1970', 'Nissie D''Alesco', 192, 3.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content4', now(), now(), 'Red-Headed Woman', 'category4', '4/13/1916', 'Dyna Maffin', 147, 2.4);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content5', now(), now(), 'Mack, The', 'category1', '8/13/1957', 'Bernelle Hinksen', 123, 7.4);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content6', now(), now(), 'Paris, France', 'category1', '8/23/1951', 'Salvatore Oakly', 205, 9.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content7', now(), now(), 'Damsel in Distress, A', 'category11', '5/13/2014', 'Brunhilde Whitney', 104, 8.1);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content8', now(), now(), 'My Son John', 'category11', '12/31/1926', 'Marillin Lorkings', 223, 2.0);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content9', now(), now(), 'Most Wanted Man, A', 'category10', '1/12/1909', 'Broderick Onyon', 73, 4.8);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content10', now(), now(), 'Kolya (Kolja)', 'category4', '10/8/1923', 'Lucia Studdert', 171, 3.4);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content11', now(), now(), 'Defying Gravity', 'category10', '11/17/1952', 'Constantin Nears', 226, 0.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content12', now(), now(), 'Postcards From the Edge', 'category10', '3/26/1902', 'Beatrisa Winsome', 79, 1.9);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content13', now(), now(), 'Trip, The', 'category3', '12/12/1925', 'Emerson Skipton', 130, 1.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content14', now(), now(), 'Seven Days to Noon', 'category3', '2/13/1972', 'Sibby Cossom', 227, 8.0);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content15', now(), now(), 'Mommy', 'category8', '1/20/2022', 'Sasha Dassindale', 67, 9.8);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content16', now(), now(), 'Thursday', 'category6', '12/26/1959', 'Kaila Syversen', 66, 4.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content17', now(), now(), 'Lonesome', 'category5', '2/15/1927', 'Elijah Clougher', 110, 7.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content18', now(), now(), 'Fade to Black', 'category10', '7/12/2002', 'Donnajean Coale', 200, 8.4);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content19', now(), now(), 'Life and Adventures of Santa Claus, The', 'category3', '12/28/1905', 'Ardath Poppleston', 186, 1.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content20', now(), now(), 'Where Is Fred!? (Wo ist Fred?)', 'category8', '4/19/1957', 'Ferdie Moden', 181, 8.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content21', now(), now(), 'V/H/S', 'category9', '4/3/1973', 'Petronella Craze', 221, 8.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content22', now(), now(), 'Bloody Angels (1732 Høtten: Marerittet har et postnummer)', 'category11', '8/17/1941', 'Tymon Dearn', 70, 7.9);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content23', now(), now(), 'Aziz Ansari: Live at Madison Square Garden', 'category7', '6/9/1956', 'Beatrice Aubury', 132, 8.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content24', now(), now(), 'Viagem a Portugal', 'category10', '5/13/1948', 'Cindee Clausen', 165, 6.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content25', now(), now(), 'Snake in the Eagle''s Shadow (Se ying diu sau)', 'category10', '12/17/1919', 'Merla Handling', 130, 9.4);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content26', now(), now(), 'Love and Anarchy (Film d''amore e d''anarchia, ovvero ''stamattina alle 10 in via dei Fiori nella nota casa di tolleranza...'')', 'category5', '1/30/1985', 'Hube Shuttell', 238, 6.9);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content27', now(), now(), 'Midnight Bayou', 'category9', '5/19/2013', 'Alanna Hinz', 207, 0.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content28', now(), now(), 'Paradise Lost 2: Revelations', 'category1', '12/24/1922', 'Hale Izatt', 65, 7.1);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content29', now(), now(), 'My Giant', 'category1', '4/18/1973', 'Mariette Lewins', 135, 3.9);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content30', now(), now(), 'Captives', 'category2', '5/8/1978', 'Ilka Illyes', 186, 4.4);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content31', now(), now(), 'Il fiore dai petali d''acciaio', 'category6', '7/28/1907', 'Wernher Poleykett', 144, 7.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content32', now(), now(), 'Call Me Bwana', 'category4', '12/6/2003', 'Ware Osbaldeston', 204, 4.1);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content33', now(), now(), 'First Blood (Rambo: First Blood)', 'category3', '4/4/1944', 'Brock Sarton', 65, 10.0);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content34', now(), now(), 'Confessions of a Window Cleaner', 'category6', '9/12/1979', 'Natty Bann', 182, 2.1);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content35', now(), now(), 'Grease', 'category1', '3/3/1949', 'Hildegaard Lorey', 232, 1.4);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content36', now(), now(), 'Bridget Jones: The Edge of Reason', 'category6', '11/18/1970', 'Shep Spurritt', 162, 7.1);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content37', now(), now(), 'Tevye', 'category8', '6/28/2011', 'Gerrilee Ephson', 90, 5.8);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content38', now(), now(), 'Heaven Can Wait', 'category7', '7/30/1945', 'Antonio Yackiminie', 187, 4.8);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content39', now(), now(), 'Kummeli Goldrush (Kummeli kultakuume)', 'category4', '9/3/2020', 'Henrie Trewett', 114, 7.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content40', now(), now(), 'Bummer (Bumer)', 'category3', '5/6/1954', 'Larissa Yushin', 202, 9.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content41', now(), now(), 'Sinbad: Legend of the Seven Seas', 'category6', '10/11/1950', 'Frederik Konertz', 106, 9.9);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content42', now(), now(), 'Harder They Fall, The', 'category3', '4/5/1928', 'Adham Paulley', 183, 0.7);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content43', now(), now(), 'Outlaw, The', 'category9', '8/3/1914', 'Sabra Gossling', 240, 9.9);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content44', now(), now(), 'Antz', 'category4', '1/12/2016', 'Barbe Redon', 74, 5.1);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content45', now(), now(), 'Taxi 3', 'category2', '8/12/1994', 'Lottie Filipiak', 189, 8.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content46', now(), now(), 'Bullet for a Badman (Renegade Posse)', 'category5', '8/18/1916', 'Skell Cino', 188, 8.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content47', now(), now(), 'Woman Chaser, The', 'category5', '11/3/1913', 'Marline Reichardt', 232, 7.1);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content48', now(), now(), 'Morning Glory', 'category4', '6/27/1918', 'Nikolia Hebditch', 120, 9.9);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content49', now(), now(), 'Don''t Move (Non ti muovere)', 'category11', '8/14/1995', 'Hettie Luggar', 126, 5.4);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content50', now(), now(), 'Safe Men', 'category6', '5/16/1967', 'Amandy Pee', 103, 8.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content51', now(), now(), 'Prime Suspect: Inner Circles', 'category7', '7/30/1923', 'Michaeline Musk', 107, 8.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content52', now(), now(), 'American Mullet', 'category6', '9/25/1973', 'Ricki Mc Carrick', 219, 2.7);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content53', now(), now(), 'Shaft''s Big Score!', 'category8', '8/12/1917', 'Elberta Melling', 160, 8.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content54', now(), now(), 'Time After Time', 'category2', '10/18/1965', 'Lanni Carnow', 220, 1.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content55', now(), now(), 'Nine Lives', 'category9', '12/29/1910', 'Valenka Bathow', 123, 1.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content56', now(), now(), 'Temptations, The', 'category9', '8/27/1981', 'Lu Strippling', 163, 9.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content57', now(), now(), 'Major League II', 'category8', '5/4/1969', 'Luis Blader', 182, 5.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content58', now(), now(), 'While the City Sleeps', 'category3', '1/10/1903', 'Rodi Gimblett', 225, 5.0);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content59', now(), now(), 'Mademoiselle', 'category2', '12/8/2014', 'Roderic Mumbray', 238, 8.1);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content60', now(), now(), 'Witch Way Love (Un amour de sorcière)', 'category8', '8/1/1973', 'Heinrik Noto', 114, 9.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content61', now(), now(), 'All Fall Down', 'category1', '12/12/1957', 'Daffi Pendreigh', 131, 6.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content62', now(), now(), 'Valhalla', 'category4', '3/16/1902', 'Lenard Fowden', 157, 2.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content63', now(), now(), 'Fox and the Hound, The', 'category9', '12/6/1956', 'Vally Blewis', 173, 3.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content64', now(), now(), 'Godfather, The', 'category2', '2/14/1990', 'Ogdan Banyard', 137, 9.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content65', now(), now(), 'Indiana Jones and the Temple of Doom', 'category9', '4/19/1984', 'Pooh Kedslie', 141, 8.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content66', now(), now(), 'Adrift in Tokyo (Tenten)', 'category2', '11/7/1955', 'Bryon Mossdale', 74, 9.0);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content67', now(), now(), 'Cheech & Chong''s The Corsican Brothers', 'category11', '10/10/1942', 'Reese Belk', 146, 7.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content68', now(), now(), 'P.U.N.K.S', 'category8', '5/21/1974', 'Randene Cheltnam', 93, 7.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content69', now(), now(), 'Love, etc.', 'category11', '4/28/1981', 'Caddric Comizzoli', 141, 2.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content70', now(), now(), 'Night of the Running Man', 'category1', '5/19/1984', 'Nadean Taber', 73, 4.8);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content71', now(), now(), 'Fahrenhype 9/11', 'category7', '7/11/1954', 'Tabby Revill', 84, 7.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content72', now(), now(), 'The Adventures of Tom Thumb & Thumbelina', 'category6', '3/6/1961', 'Hamilton Lehrmann', 88, 0.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content73', now(), now(), 'Georgia', 'category9', '3/9/1961', 'Gladys Imlin', 123, 4.0);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content74', now(), now(), 'Kiss the Bride', 'category4', '8/13/2012', 'Lauryn Arntzen', 158, 8.6);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content75', now(), now(), 'Pay It Forward', 'category4', '2/25/1915', 'Niel Faivre', 158, 0.7);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content76', now(), now(), 'Kawasaki''s Rose ', 'category9', '1/17/1928', 'Bronny Killingbeck', 187, 9.0);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content77', now(), now(), 'No Man of Her Own ', 'category7', '7/21/1946', 'Emmalee Ulrik', 200, 5.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content78', now(), now(), 'Glass Slipper, The', 'category11', '12/11/1900', 'Nariko Hedges', 174, 6.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content79', now(), now(), 'Talhotblond:', 'category10', '11/23/2020', 'Christie Koppelmann', 106, 9.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content80', now(), now(), 'Dying Gaul, The', 'category9', '7/15/1980', 'Brittan Carlozzi', 227, 4.7);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content81', now(), now(), 'Grapes of Wrath, The', 'category8', '8/17/1958', 'Gordon Houlson', 233, 3.7);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content82', now(), now(), 'Tale of Tales (Skazka skazok)', 'category4', '3/24/1966', 'Cayla Alennikov', 104, 1.8);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content83', now(), now(), 'Melody Time', 'category4', '8/19/1972', 'Odele Heinecke', 141, 4.7);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content84', now(), now(), 'Mystery of the Wax Museum', 'category7', '11/2/1937', 'Krysta Kalkofen', 129, 4.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content85', now(), now(), 'Fuga de cerebros', 'category7', '7/2/1936', 'Petr Bree', 144, 2.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content86', now(), now(), 'North to Alaska', 'category2', '6/9/2021', 'Em Clarabut', 64, 0.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content87', now(), now(), 'Audrey Rose', 'category6', '1/6/1999', 'Stesha Bannell', 150, 2.8);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content88', now(), now(), 'Fist of Fury (Chinese Connection, The) (Jing wu men)', 'category2', '1/9/1909', 'Forest Cattlemull', 183, 7.4);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content89', now(), now(), 'Dear Zachary: A Letter to a Son About His Father', 'category3', '9/25/1910', 'Dee dee Eede', 115, 7.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content90', now(), now(), 'Austenland', 'category9', '1/17/2014', 'Perren Inglesfield', 226, 8.7);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content91', now(), now(), 'Marketa Lazarová', 'category2', '7/23/2022', 'Berty Grog', 163, 0.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content92', now(), now(), 'Just Friends?', 'category3', '2/29/1944', 'Jolynn Dimitrie', 154, 2.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content93', now(), now(), 'I''m Still Here', 'category2', '2/6/1908', 'Beverley McKenzie', 233, 1.5);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content94', now(), now(), 'Marius', 'category5', '1/29/1930', 'Quent Waren', 210, 3.7);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content95', now(), now(), 'Motivation, The', 'category1', '5/24/1940', 'Rosella Manby', 210, 2.8);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content96', now(), now(), 'Muppet Family Christmas, A', 'category5', '11/2/1974', 'Calvin O''Haire', 68, 5.3);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content97', now(), now(), 'Three Musketeers, The', 'category9', '1/4/1997', 'Garret Mitcham', 156, 8.8);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content98', now(), now(), 'Love on the Run (Amour en fuite, L'')', 'category11', '6/3/1904', 'Nolana Tremollet', 125, 6.2);
insert into contents (id, created, updated, title, genre_id, release_date, director, duration, rating) values ('content99', now(), now(), 'Antiviral', 'category9', '11/15/1914', 'Delbert Heathcoat', 223, 5.5);

insert into users_starred_categories (followers_id, starred_categories_id) values ('user3', 'category4');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user5', 'category11');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user10', 'category1');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user8', 'category3');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user8', 'category1');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user7', 'category2');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user9', 'category4');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user1', 'category11');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user10', 'category6');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user10', 'category8');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user5', 'category1');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user7', 'category1');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user6', 'category9');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user3', 'category2');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user1', 'category4');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user9', 'category7');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user1', 'category3');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user8', 'category4');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user4', 'category10');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user5', 'category9');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user10', 'category4');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user6', 'category6');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user10', 'category3');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user10', 'category7');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user4', 'category1');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user3', 'category7');
insert into users_starred_categories (followers_id, starred_categories_id) values ('user2', 'category5');


insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content99');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content57');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user2', 'content86');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content47');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user9', 'content33');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user2', 'content3');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user6', 'content24');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content6');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content17');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user6', 'content94');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user4', 'content29');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content60');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content16');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content14');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content56');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user9', 'content99');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content27');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content13');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content37');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content79');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content89');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content65');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content88');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content84');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user9', 'content86');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content47');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user4', 'content62');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content73');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user4', 'content73');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content6');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user2', 'content32');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user10', 'content9');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content34');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user6', 'content35');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content42');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user10', 'content97');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user6', 'content31');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user4', 'content48');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content85');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user9', 'content35');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user7', 'content23');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user2', 'content29');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content37');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content94');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content37');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user4', 'content21');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user9', 'content48');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user4', 'content55');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content12');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content37');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user9', 'content24');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user6', 'content26');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content70');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content39');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content7');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content7');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user2', 'content61');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content72');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content29');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user9', 'content35');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content66');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user6', 'content7');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user6', 'content28');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content12');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user4', 'content19');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user7', 'content0');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content6');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content39');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content37');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user3', 'content64');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content88');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user7', 'content91');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content84');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user2', 'content62');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content7');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user6', 'content77');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content57');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user10', 'content46');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user9', 'content21');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user9', 'content50');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user2', 'content54');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content26');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content68');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user5', 'content87');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user10', 'content96');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content77');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content1');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content15');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user9', 'content10');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content52');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user1', 'content33');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user4', 'content71');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content33');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content22');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user2', 'content4');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content97');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content72');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user2', 'content38');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user2', 'content56');
insert into users_starred_contents (followers_id, starred_contents_id) values ('user8', 'content29');

insert into pricing_schedules (id,created,updated,active,amount,discount, payment_period, repetition_count) values ('pricing_schedule1', now(), now(), true, 100, 0, 30, 1);
insert into pricing_schedules (id,created,updated,active,amount,discount, payment_period, repetition_count) values ('pricing_schedule2', now(), now(), true, 90, 0, 30, 3);
insert into pricing_schedules (id,created,updated,active,amount,discount, payment_period, repetition_count) values ('pricing_schedule3', now(), now(), true, 80, 0, 30, 5);
insert into pricing_schedules (id,created,updated,active,amount,discount, payment_period, repetition_count) values ('pricing_schedule4', now(), now(), true, 800, 0, 365, 1);