package com.researchecosystems.contentserviceapi.repository;

import com.researchecosystems.contentserviceapi.entity.PricingSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PricingScheduleRepository  extends JpaRepository<PricingSchedule, String> {

    List<PricingSchedule> findAllByActive(boolean active);

    Optional<PricingSchedule> findByIdAndActive(String id, boolean active);
}
