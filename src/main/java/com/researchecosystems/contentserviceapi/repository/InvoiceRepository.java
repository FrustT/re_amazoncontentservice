package com.researchecosystems.contentserviceapi.repository;

import com.researchecosystems.contentserviceapi.entity.Invoice;
import com.researchecosystems.contentserviceapi.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
public interface InvoiceRepository  extends JpaRepository<Invoice, String> {
    List<Invoice> findAllByUser(User user);
    List<Invoice> findAllByUserAndPaid(User user, boolean paid);

    List<Invoice> findAllByPaidAndDueDateIsBefore(boolean paid, ZonedDateTime dueDate);

    List<Invoice> findAllByActiveAndActivationDateBefore(boolean active, ZonedDateTime activationDate);
}
