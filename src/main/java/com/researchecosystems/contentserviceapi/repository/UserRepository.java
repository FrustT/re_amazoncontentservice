package com.researchecosystems.contentserviceapi.repository;

import com.researchecosystems.contentserviceapi.entity.Category;
import com.researchecosystems.contentserviceapi.entity.Content;
import com.researchecosystems.contentserviceapi.entity.User;
import com.researchecosystems.contentserviceapi.entity.UserRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    Optional<User> findFirstByEmail(String email);

    List<User> findAllByStarredCategoriesContaining(Category category);
    Page<User> findAllByStarredCategoriesContaining(Category category, Pageable pageable);
    List<User> findAllByStarredContentsContaining(Content content);
    Page<User> findAllByStarredContentsContaining(Content content, Pageable pageable);
    boolean existsByEmail(String email);

    User findByEmail(String email);

}
