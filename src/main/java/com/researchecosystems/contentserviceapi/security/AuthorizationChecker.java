package com.researchecosystems.contentserviceapi.security;

import com.researchecosystems.contentserviceapi.entity.User;
import com.researchecosystems.contentserviceapi.entity.UserRole;
import com.researchecosystems.contentserviceapi.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthorizationChecker {

    private final AuthenticationService authenticationService;
    @Autowired
    public AuthorizationChecker(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public boolean hasUserRoleOrSelfCompoundCheck(String requestedUserId, UserRole neededUserRole){
        User authenticatedUser = getAuthenticatedUser();
        String authenticatedUserId = authenticatedUser.getId();
        UserRole authenticatedUserRole = authenticatedUser.getUserRole();

        return passesSelfCheck(requestedUserId,authenticatedUserId) || passesRoleCheck(neededUserRole,authenticatedUserRole) ;
    }
    public boolean hasUserRole(UserRole neededUserRole){
        User authenticatedUser = getAuthenticatedUser();
        UserRole authenticatedUserRole = authenticatedUser.getUserRole();

        return passesRoleCheck(neededUserRole,authenticatedUserRole);
    }
    public boolean hasDifferentUserRoleThan(UserRole unwantedUserRole){
        User authenticatedUser = getAuthenticatedUser();
        UserRole authenticatedUserRole = authenticatedUser.getUserRole();

        return  !passesRoleCheck(unwantedUserRole,authenticatedUserRole);
    }
    public boolean hasSameId(String requestedUserId) {
        User authenticatedUser = getAuthenticatedUser();
        String authenticatedUserId = authenticatedUser.getId();

        return passesSelfCheck(requestedUserId,authenticatedUserId);
    }
    public boolean passesRoleCheck(UserRole neededUserRole, UserRole authenticatedUserRole){
        return authenticatedUserRole.equals(neededUserRole);
    }
    public boolean passesSelfCheck(String requestedUserId, String authenticatedUserId) {
        return authenticatedUserId.equals(requestedUserId);
    }
    public User getAuthenticatedUser() {
        Optional<User> dbUserResponse = authenticationService.getAuthenticatedUser();

        if (dbUserResponse.isPresent()) {
            return dbUserResponse.get();
        }else {
            throw new UsernameNotFoundException("User not found");
        }
    }
}
