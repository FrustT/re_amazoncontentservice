package com.researchecosystems.contentserviceapi.config;

import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

@AllArgsConstructor
@Configuration
public class ConfigRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

    }
}
