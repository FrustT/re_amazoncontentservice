package com.researchecosystems.contentserviceapi.model.response.category;

import com.researchecosystems.contentserviceapi.entity.Category;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class CategoryResponse {

    private String id;
    private ZonedDateTime created;
    private ZonedDateTime updated;
    private String name;
    private String description;
    private int subCategoryCount;
    private int contentCount;
    private int followerCount;

    public static CategoryResponse fromEntity(Category category) {
        return CategoryResponse.builder()
                .id(category.getId())
                .created(category.getCreated())
                .updated(category.getUpdated())
                .name(category.getName())
                .description(category.getDescription())
                .contentCount(category.getContentCount())
                .followerCount(category.getUserCount())
                .subCategoryCount(category.getSubCategoryCount())
                .build();
    }
}
