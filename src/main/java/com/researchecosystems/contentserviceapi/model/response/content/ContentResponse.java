package com.researchecosystems.contentserviceapi.model.response.content;

import com.researchecosystems.contentserviceapi.entity.Category;
import com.researchecosystems.contentserviceapi.entity.Content;
import com.researchecosystems.contentserviceapi.model.response.category.CategoryResponse;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class ContentResponse {

    private String id;
    private ZonedDateTime created;
    private ZonedDateTime updated;
    private String title;
    private CategoryResponse genre;
    private String releaseDate;
    private int duration;
    private float rating;
    private String director;
    private int followerCount;

    public static ContentResponse fromEntity(Content content) {
        return ContentResponse.builder()
                .id(content.getId())
                .created(content.getCreated())
                .updated(content.getUpdated())
                .title(content.getTitle())
                .genre(CategoryResponse.fromEntity(content.getGenre()))
                .releaseDate(content.getReleaseDate())
                .duration(content.getDuration())
                .rating(content.getRating())
                .director(content.getDirector())
                .followerCount(content.getFollowerCount())
                .build();
    }

}
