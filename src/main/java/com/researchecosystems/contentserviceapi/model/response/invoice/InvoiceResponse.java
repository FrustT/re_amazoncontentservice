package com.researchecosystems.contentserviceapi.model.response.invoice;

import com.researchecosystems.contentserviceapi.entity.Invoice;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class InvoiceResponse {
    private String id;
    private int amount ;
    private boolean paid;
    private boolean active;
    private ZonedDateTime dueDate;
    private ZonedDateTime activationDate;

    public static InvoiceResponse fromEntity(Invoice invoice) {
        return InvoiceResponse.builder()
                .id(invoice.getId())
                .amount(invoice.getAmount())
                .paid(invoice.isPaid())
                .active(invoice.isActive())
                .dueDate(invoice.getDueDate())
                .activationDate(invoice.getActivationDate())
                .build();
    }
}
