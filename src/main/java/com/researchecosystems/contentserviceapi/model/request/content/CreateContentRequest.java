package com.researchecosystems.contentserviceapi.model.request.content;

import com.researchecosystems.contentserviceapi.entity.Category;
import com.researchecosystems.contentserviceapi.entity.Content;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Data
@ToString
public class CreateContentRequest {

    public CreateContentRequest(Content content){
        this.title = content.getTitle();
        this.genre = content.getGenre();
        this.releaseDate = content.getReleaseDate();
        this.duration = content.getDuration();
        this.rating = content.getRating();
        this.director = content.getDirector();
    }
    @NotNull
    private String title;
    @NotNull
    private Category genre;
    @NotNull
    private String releaseDate;
    @NotNull
    private int duration;
    @NotNull
    private float rating;
    @NotNull
    private String director;
}
