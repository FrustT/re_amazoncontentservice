package com.researchecosystems.contentserviceapi.model.request.auth;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
public class PasswordChangeRequest {

    @NotEmpty(message = "New Password must be filled.")
    @Length(min = 6)
    private String newPassword;

    @NotEmpty(message = "New Password again must be filled.")
    @Length(min = 6)
    private String newPasswordAgain;
}
