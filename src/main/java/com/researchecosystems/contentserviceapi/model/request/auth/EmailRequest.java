package com.researchecosystems.contentserviceapi.model.request.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@Getter
@Setter
public class EmailRequest {

    @Email
    @NotEmpty
    private String email;

}
