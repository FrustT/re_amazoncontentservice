package com.researchecosystems.contentserviceapi.model.request.auth;

import lombok.*;
import org.hibernate.validator.constraints.Length;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@Getter
@Setter
public class LoginRequest {

    @Email
    @NotEmpty
    private String email;

    @Length(min = 6)
    private String password;

}
