package com.researchecosystems.contentserviceapi.model.request.category;

import com.researchecosystems.contentserviceapi.entity.Category;
import lombok.Data;
import lombok.ToString;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class CreateCategoryRequest {

    public CreateCategoryRequest(Category category){
        this.name = category.getName();
        this.description = category.getDescription();
        this.parentCategory = category.getParentCategory();
    }

    @NotNull
    private String name;
    @NotNull
    private String description;
    private Category parentCategory;

}
