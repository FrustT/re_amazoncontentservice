package com.researchecosystems.contentserviceapi.model.request.auth.register;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@AllArgsConstructor
@Getter
@Setter
public class RegisterRequest {

    @Email(message = "Invalid Email!")
    @NotEmpty(message = "Email must be filled!")
    private String email;

    @Size(min = 6)
    @NotEmpty(message = "Password must be filled!")
    private String password;

}