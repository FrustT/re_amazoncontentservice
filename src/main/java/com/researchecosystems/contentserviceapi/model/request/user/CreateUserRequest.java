package com.researchecosystems.contentserviceapi.model.request.user;

import com.researchecosystems.contentserviceapi.entity.User;
import com.researchecosystems.contentserviceapi.entity.UserRole;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ToString
public class CreateUserRequest {

    public CreateUserRequest(User user){
        this.email = user.getEmail();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.userRole = user.getUserRole();
    }

    @Email(message = "Geçersiz e-posta adresi.")
    @NotEmpty(message = "Mail adresi boş bırakılamaz!")
    private String email;

    @NotEmpty(message = "İsim boş bırakılamaz!")
    private String name;

    @NotEmpty(message = "Soyisim boş bırakılamaz!")
    private String surname;

    @NotNull(message = "Kullanıcı rolü boş bırakılamaz!")
    private UserRole userRole;

}
