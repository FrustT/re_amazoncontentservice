package com.researchecosystems.contentserviceapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.researchecosystems.contentserviceapi.repository.CategoryRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Table(name = "categories")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Category extends BaseEntity {

    private boolean isRoot = this.parentCategory == null;

    @ManyToOne
    private Category parentCategory;

    @OneToMany(mappedBy = "parentCategory")
    private List<Category> subCategories;

    @OneToMany(mappedBy = "genre")
    private List<Content> Contents;

    private String name;

    private String description;

    @ManyToMany(mappedBy = "starredCategories")
    private List<User> followers;

    public int getContentCount() {
        return Contents.size();
    }
    public int getUserCount() {
        return followers.size();
    }
    public int getSubCategoryCount() {
        return subCategories.size();
    }
}
