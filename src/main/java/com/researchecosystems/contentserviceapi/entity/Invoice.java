package com.researchecosystems.contentserviceapi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.ZonedDateTime;

@Table(name = "invoices")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Invoice extends BaseEntity {

    @ManyToOne
    private User user;

    private int amount;

    private int paymentPeriod;

    private boolean paid;

    private boolean active;

    private ZonedDateTime paidDate;

    private ZonedDateTime activationDate;

    private ZonedDateTime dueDate;
}
