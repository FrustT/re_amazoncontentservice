package com.researchecosystems.contentserviceapi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Table(name = "pricing_schedules")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PricingSchedule extends BaseEntity{


    private int amount;

    private int paymentPeriod;

    private int discount;

    private boolean active;

    private int repetitionCount;

    public List<Invoice> createInvoices(){
        List<Invoice> invoices = new ArrayList<>();

        ZonedDateTime currentActivationDate = ZonedDateTime.now();
        for (int i = 0; i < repetitionCount; i++) {
            Invoice invoice = new Invoice();
            invoice.setAmount(amount);
            invoice.setPaymentPeriod(paymentPeriod);
            invoice.setActive(false);
            invoice.setPaid(false);
            invoice.setActivationDate(currentActivationDate);
            currentActivationDate = currentActivationDate.plusDays(paymentPeriod);

            invoice.setDueDate(currentActivationDate);

            invoices.add(invoice);
        }
        return invoices;
    }

}
