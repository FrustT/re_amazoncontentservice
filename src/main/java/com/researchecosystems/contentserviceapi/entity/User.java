package com.researchecosystems.contentserviceapi.entity;

import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Table(name = "users")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "email")
    private String email;

    @Column(name = "password_hash")
    private String passwordHash;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @Column(name = "verification_code")
    private String verificationCode;

    @Column(name = "verification_code_expiration_date")
    private ZonedDateTime verificationCodeExpirationDate;

    @Column(name = "recovery_code")
    private String recoveryCode;

    @Column(name = "recovery_code_expiration_date")
    private ZonedDateTime recoveryCodeExpirationDate;

    @Column(name = "is_verified")
    private boolean verified;

    @ManyToMany
    private List<Category> starredCategories;

    @ManyToMany
    private List<Content> starredContents;

    @OneToMany(mappedBy = "user")
    private List<Invoice> invoices;

    public int getStarredCategoryCount() {
        return starredCategories.size();
    }
    public int getStarredContentCount() {
        return starredContents.size();
    }

}
