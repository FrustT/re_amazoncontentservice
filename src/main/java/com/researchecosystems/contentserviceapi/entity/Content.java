package com.researchecosystems.contentserviceapi.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Table(name = "contents")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Content extends BaseEntity {

        @Column(name = "title")
        private String title;

        @ManyToOne
        private Category genre;
        @Column(name = "release_date")
        private String releaseDate;
        @Column(name = "duration")
        private int duration;
        @Column(name = "rating")
        private float rating;
        @Column(name = "director")
        private String director;

        @ManyToMany(mappedBy = "starredContents")
        private List<User> followers;

        public int getFollowerCount() {
                return followers.size();
        }


}
