package com.researchecosystems.contentserviceapi.controller;

import com.researchecosystems.contentserviceapi.entity.Invoice;
import com.researchecosystems.contentserviceapi.entity.PricingSchedule;
import com.researchecosystems.contentserviceapi.model.response.invoice.InvoiceResponse;
import com.researchecosystems.contentserviceapi.service.InvoiceService;
import lombok.AllArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@RequestMapping("/invoice")
public class InvoiceController {

    private final InvoiceService invoiceService;

    @GetMapping
    public Page<InvoiceResponse> listInvoices(@ParameterObject Pageable pageable) {
        return invoiceService.listInvoices(pageable);
    }

    @GetMapping("/{userId}")
    public List<InvoiceResponse> listUserInvoices(@PathVariable String userId) {
        return invoiceService.getInvoicesOfUser(userId).stream().map(InvoiceResponse::fromEntity).collect(Collectors.toList());
    }
    @PostMapping("/processInvoices")
    public String processInvoices() {
        invoiceService.processInvoices();
        return "Invoices have been processed";
    }
    @PostMapping("/{invoiceId}/pay")
    public InvoiceResponse payInvoice(@PathVariable String invoiceId){
        return invoiceService.payInvoice(invoiceId);

    }
    @PostMapping("/{userId}/subscribe/{PricingScheduleId}")
    public String subscribe(@PathVariable String userId, @PathVariable String PricingScheduleId){
        invoiceService.subscribe(userId, PricingScheduleId);
        return "User has been subscribed";
    }

    @GetMapping("/plans")
    public List<PricingSchedule> listPlans(){
        return invoiceService.listPlans();
    }



}
