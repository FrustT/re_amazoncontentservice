package com.researchecosystems.contentserviceapi.controller;

import com.researchecosystems.contentserviceapi.model.request.auth.*;
import com.researchecosystems.contentserviceapi.model.request.auth.register.RegisterRequest;
import com.researchecosystems.contentserviceapi.model.response.LoginResponse;
import com.researchecosystems.contentserviceapi.service.AuthenticationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@Validated
@AllArgsConstructor
public class AuthController {

    private final AuthenticationService authenticationService;


    @PostMapping("/register")
    public ResponseEntity<String> register(@Valid @RequestBody RegisterRequest request) {
        authenticationService.register(request);
        return ResponseEntity.ok("User Registered");
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@Valid @RequestBody LoginRequest request) {
        return ResponseEntity.ok(authenticationService.login(request));
    }
    @GetMapping("/verify/{username}")
    public ResponseEntity<String> verify(@RequestParam("code") String verificationCode, @PathVariable("username") String username) {
        authenticationService.verify(username,verificationCode);
        return ResponseEntity.ok("User Verified");
    }

    @PostMapping("/verify/{username}")
    public ResponseEntity<String> verifyByEmail( @PathVariable("username") String username) {
        String link = authenticationService.sendVerificationEmail(username);
        return ResponseEntity.ok("Verification Link has been sent to your email " + link);
    }

    @PutMapping("/recover/{username}")
    public ResponseEntity<String> recover( @PathVariable("username") String username, @RequestParam("code") String recoveryCode,@Valid @RequestBody PasswordChangeRequest request) {
        System.out.println("username: " + username +" recoveryCode: " + recoveryCode + " newPassword: " + request.getNewPassword()+"newPasswordAgain: " + request.getNewPasswordAgain());
        authenticationService.resetPassword(username,recoveryCode, request);
        return ResponseEntity.ok("Password Changed");
    }
    @PostMapping("/recover/{username}")
    public ResponseEntity<String> recoverRequest( @PathVariable("username") String username) {
        String link = authenticationService.recoverPasswordRequest(username);
        return ResponseEntity.ok("Recovery Link has been sent to your email " + link);
    }

}
