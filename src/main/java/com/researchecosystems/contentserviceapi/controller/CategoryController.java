package com.researchecosystems.contentserviceapi.controller;

import com.researchecosystems.contentserviceapi.entity.Category;
import com.researchecosystems.contentserviceapi.model.request.category.CreateCategoryRequest;
import com.researchecosystems.contentserviceapi.model.request.category.UpdateCategoryRequest;
import com.researchecosystems.contentserviceapi.model.response.category.CategoryResponse;
import com.researchecosystems.contentserviceapi.model.response.content.ContentResponse;
import com.researchecosystems.contentserviceapi.model.response.user.UserResponse;
import com.researchecosystems.contentserviceapi.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/category")
public class CategoryController {

    private CategoryService categoryService;
    @GetMapping
    public Page<CategoryResponse> listCategories(@ParameterObject Pageable pageable){
        return categoryService.listCategories(pageable);
    }
    @GetMapping("/{categoryId}")
    public CategoryResponse getCategory(@PathVariable String categoryId){
        return CategoryResponse.fromEntity(categoryService.getCategory(categoryId));
    }
    @GetMapping("/{categoryId}/contents")
    public List<ContentResponse> getCategoryContents(@PathVariable String categoryId){
        return categoryService.getCategoryContents(categoryId);
    }
    @GetMapping("/{categoryId}/followers")
    public Page<UserResponse> getCategoryFollowers(@PathVariable String categoryId, @ParameterObject Pageable pageable){
        return categoryService.getCategoryFollowers(categoryId, pageable);
    }
    @PostMapping
    public CategoryResponse createCategory(@Valid @RequestBody CreateCategoryRequest request){
        return categoryService.createCategory(request);
    }
    @PutMapping("/{categoryId}")
    public CategoryResponse updateCategory(@PathVariable String categoryId, @Valid @RequestBody UpdateCategoryRequest request){
        return categoryService.updateCategory(categoryId, request);
    }
    @DeleteMapping("/{categoryId}")
    public String deleteCategory(@PathVariable String categoryId){
        categoryService.deleteCategory(categoryId);
        return "Category has been deleted";
    }
}
