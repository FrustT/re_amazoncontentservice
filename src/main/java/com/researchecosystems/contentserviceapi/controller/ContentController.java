package com.researchecosystems.contentserviceapi.controller;

import com.researchecosystems.contentserviceapi.entity.Content;
import com.researchecosystems.contentserviceapi.model.request.content.CreateContentRequest;
import com.researchecosystems.contentserviceapi.model.request.content.UpdateContentRequest;
import com.researchecosystems.contentserviceapi.model.response.content.ContentResponse;
import com.researchecosystems.contentserviceapi.model.response.user.UserResponse;
import com.researchecosystems.contentserviceapi.service.ContentService;
import lombok.AllArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/content")
public class ContentController {

    private final ContentService contentService;
    @GetMapping
    public Page<ContentResponse> getContents(@ParameterObject Pageable pageable) {
        return contentService.listContents(pageable);
    }

    @PreAuthorize("@authorizationChecker.hasDifferentUserRoleThan('GUEST')")
    @GetMapping("/{contentId}")
    public ContentResponse getContent(@PathVariable String contentId){
        return ContentResponse.fromEntity(contentService.getContent(contentId));
    }
    @PreAuthorize("@authorizationChecker.hasUserRole('ADMIN')")
    @GetMapping("/{contentId}/followers")
    public Page<UserResponse> getContentFollowers(@PathVariable String contentId, @ParameterObject Pageable pageable){
        return contentService.getContentFollowers(contentId, pageable);
    }

    @PostMapping
    @PreAuthorize("@authorizationChecker.hasUserRole('ADMIN')")
    public ContentResponse createContent(@Valid @RequestBody CreateContentRequest request){
        return  contentService.createContent(request);
    }

    @PutMapping("/{contentId}")
    @PreAuthorize("@authorizationChecker.hasUserRole('ADMIN')")
    public ContentResponse updateContent(@PathVariable String contentId, @Valid @RequestBody UpdateContentRequest request){
        contentService.updateContent(contentId, request);
        return getContent(contentId);
    }

    @PreAuthorize("@authorizationChecker.hasUserRole('ADMIN')")
    @DeleteMapping("/{contentId}")
    public String deleteContent(@PathVariable String contentId){
        contentService.deleteContent(contentId);
        return "Content has been deleted";
    }
}
