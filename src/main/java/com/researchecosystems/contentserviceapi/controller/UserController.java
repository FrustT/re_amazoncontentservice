package com.researchecosystems.contentserviceapi.controller;

import com.researchecosystems.contentserviceapi.entity.User;
import com.researchecosystems.contentserviceapi.model.request.user.CreateUserRequest;
import com.researchecosystems.contentserviceapi.model.request.user.UpdateUserRequest;
import com.researchecosystems.contentserviceapi.model.response.user.UserResponse;
import com.researchecosystems.contentserviceapi.service.AuthenticationService;
import com.researchecosystems.contentserviceapi.service.UserService;
import lombok.AllArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final AuthenticationService authenticationService;

    @PreAuthorize("@authorizationChecker.hasUserRole('ADMIN')")
    @GetMapping
    public Page<UserResponse> listUsers(@ParameterObject Pageable pageable) {
        return userService.listUsers(pageable);
    }

    @PreAuthorize("@authorizationChecker.hasUserRoleOrSelfCompoundCheck(#id, 'ADMIN')")
    @GetMapping("/{userId}")
    public UserResponse getUser(@PathVariable @P("id") String userId) {
        User user = userService.getUser(userId);
        return UserResponse.fromEntity(user);
    }

    @PostMapping
    public UserResponse createUser(@Valid @RequestBody CreateUserRequest createUserRequest) {
        return userService.createUser(createUserRequest);
    }

    @PreAuthorize("@authorizationChecker.hasUserRoleOrSelfCompoundCheck(#id, 'ADMIN')")
    @PutMapping("/{userId}")
    public UserResponse updateUser(@PathVariable @P("id") String userId, @Valid @RequestBody UpdateUserRequest updateUserRequest) {
        return userService.updateUser(userId, updateUserRequest);
    }

    @PreAuthorize("@authorizationChecker.hasUserRoleOrSelfCompoundCheck(#id, 'ADMIN')")
    @DeleteMapping("/{userId}")
    public UserResponse deleteUser(@PathVariable @P("id") String userId) {
        return userService.deleteUser(userId);
    }


}