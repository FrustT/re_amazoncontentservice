package com.researchecosystems.contentserviceapi.service;

import com.researchecosystems.contentserviceapi.entity.Category;
import com.researchecosystems.contentserviceapi.entity.Content;
import com.researchecosystems.contentserviceapi.entity.User;
import com.researchecosystems.contentserviceapi.exception.BusinessException;
import com.researchecosystems.contentserviceapi.exception.ErrorCode;
import com.researchecosystems.contentserviceapi.model.request.category.CreateCategoryRequest;
import com.researchecosystems.contentserviceapi.model.request.category.UpdateCategoryRequest;
import com.researchecosystems.contentserviceapi.model.response.category.CategoryResponse;
import com.researchecosystems.contentserviceapi.model.response.content.ContentResponse;
import com.researchecosystems.contentserviceapi.model.response.user.UserResponse;
import com.researchecosystems.contentserviceapi.repository.CategoryRepository;
import com.researchecosystems.contentserviceapi.repository.ContentRepository;
import com.researchecosystems.contentserviceapi.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CategoryService {

    private CategoryRepository categoryRepository;
    private ContentRepository contentRepository;
    private UserRepository userRepository;
    //Rather than Transactional and relying on hibernate to deal with the relations
    // we can manually deal with the relations to achieve loose coupling
    //this is why this class has both category and content repositories in it.

    public Page<CategoryResponse> listCategories(Pageable pageable) {
        return categoryRepository.findAll(pageable).map(CategoryResponse::fromEntity);
    }

    public Category getCategory(String categoryId){
        return categoryRepository.findById(categoryId)
             .orElseThrow(() -> new BusinessException(ErrorCode.resource_missing, "resource not found"));
    }
    public List<ContentResponse> getCategoryContents(String categoryId){
        Category category = getCategory(categoryId);
        List<Content> contents = contentRepository.findAllByGenre(category);
        return contents.stream().map(ContentResponse::fromEntity).collect(Collectors.toList());
    }
    public Page<UserResponse> getCategoryFollowers(String categoryId, Pageable pageable){
        Category category = getCategory(categoryId);
        return userRepository.findAllByStarredCategoriesContaining(category, pageable).map(UserResponse::fromEntity);
    }

    public CategoryResponse createCategory(CreateCategoryRequest request){

        if(categoryRepository.existsByName(request.getName())){
            throw new BusinessException(ErrorCode.conflict, "resource already exists");
        }
        Category category = new Category();
        category.setName(request.getName());
        category.setDescription(request.getDescription());
        category.setParentCategory(request.getParentCategory());
        return CategoryResponse.fromEntity(categoryRepository.save(category));
    }

    public CategoryResponse updateCategory(String categoryId, UpdateCategoryRequest request){
        Category category = getCategory(categoryId);

        category.setName(request.getName());
        category.setDescription(request.getDescription());
        category.setParentCategory(request.getParentCategory());
        return CategoryResponse.fromEntity(categoryRepository.save(category));
    }


    public void deleteCategory(String categoryId){
        Category category = getCategory(categoryId);

        List<Content> contents = contentRepository.findAllByGenre(category);
        List<User> followers = userRepository.findAllByStarredCategoriesContaining(category);
        List<Category> subCategories = categoryRepository.findAllByParentCategory(category);

        category.setContents(contents);
        category.setFollowers(followers);
        category.setSubCategories(subCategories);

        if(!category.getContents().isEmpty()) {
            if (category.isRoot()) {
                throw new BusinessException(ErrorCode.conflict, "root categories with content(s) cannot be deleted, please move the content(s) to another category or delete them.");
            }
            moveContentsToParentCategory(category);
        }
        if(!category.getSubCategories().isEmpty()){
            if(category.isRoot()){
                throw new BusinessException(ErrorCode.conflict, "root categories with sub-category(ies) cannot be deleted, please move the sub-category(ies) to another category or delete them.");
            }
            moveSubCategoriesToParentCategory(category);
        }
        if(!category.getFollowers().isEmpty()){
            removeFollowersOfCategory(category);
        }
        categoryRepository.delete(category);
    }
    public void addContentsToCategory(Category category,List<Content> contents){
        contentRepository.saveAll(contents.stream().peek(content -> content.setGenre(category)).collect(Collectors.toList()));
    }


    public void moveContentsToParentCategory(Category category){
        if(category.isRoot()){
            return;
        }
        Category parentCategory = category.getParentCategory();
        List<Content> contents = category.getContents().stream().peek(content -> content.setGenre(parentCategory)).collect(Collectors.toList());

        contentRepository.saveAll(contents);

    }
    public void moveSubCategoriesToParentCategory(Category category){
        if(category.isRoot()){
            return;
        }
        if(category.getSubCategories().isEmpty()){
            return;
        }
        Category parentCategory = category.getParentCategory();
        List<Category> subCategories = categoryRepository.findAllByParentCategory(category);
        categoryRepository.saveAll(subCategories.stream().peek(subCategory -> subCategory.setParentCategory(parentCategory)).collect(Collectors.toList()));
    }
    public void removeFollowersOfCategory(Category category){
        List<User> followers = userRepository.findAllByStarredCategoriesContaining(category);

    userRepository.saveAll(followers.stream().peek(follower -> follower.getStarredCategories().remove(category)).collect(Collectors.toList()));

    }

}
