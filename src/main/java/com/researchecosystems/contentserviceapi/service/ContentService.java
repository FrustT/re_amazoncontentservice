package com.researchecosystems.contentserviceapi.service;

import com.researchecosystems.contentserviceapi.entity.Content;
import com.researchecosystems.contentserviceapi.entity.User;
import com.researchecosystems.contentserviceapi.exception.BusinessException;
import com.researchecosystems.contentserviceapi.exception.ErrorCode;
import com.researchecosystems.contentserviceapi.model.request.content.CreateContentRequest;
import com.researchecosystems.contentserviceapi.model.request.content.UpdateContentRequest;
import com.researchecosystems.contentserviceapi.model.response.category.CategoryResponse;
import com.researchecosystems.contentserviceapi.model.response.content.ContentResponse;
import com.researchecosystems.contentserviceapi.model.response.user.UserResponse;
import com.researchecosystems.contentserviceapi.repository.ContentRepository;
import com.researchecosystems.contentserviceapi.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ContentService {

    private final ContentRepository contentRepository;
    private final UserRepository userRepository;

    public Page<ContentResponse> listContents(Pageable pageable) {
        return contentRepository.findAll(pageable).map(ContentResponse::fromEntity);
    }
    public Content getContent(String contentId){
        return contentRepository.findById(contentId)
             .orElseThrow(() -> new BusinessException(ErrorCode.resource_missing, "resource not found"));
    }
    public Page<UserResponse> getContentFollowers(String contentId, Pageable pageable){
        Content content = getContent(contentId);

        return userRepository.findAllByStarredContentsContaining(content, pageable).map(UserResponse::fromEntity);
    }
    public ContentResponse createContent(CreateContentRequest request){
        Content newContent = new Content();
        newContent.setDirector(request.getDirector());
        newContent.setDuration(request.getDuration());
        newContent.setTitle(request.getTitle());
        newContent.setRating(request.getRating());
        newContent.setReleaseDate(request.getReleaseDate());
        contentRepository.save(newContent);

        return ContentResponse.fromEntity(newContent);
    }
    public void updateContent(String contentId, UpdateContentRequest request) {
        Content contentToUpdate = getContent(contentId);
        contentToUpdate.setDirector(request.getDirector());
        contentToUpdate.setDuration(request.getDuration());
        contentToUpdate.setTitle(request.getTitle());
        contentToUpdate.setRating(request.getRating());
        contentToUpdate.setReleaseDate(request.getReleaseDate());
        contentRepository.save(contentToUpdate);
    }

    public void deleteContent(String contentId){
        Content contentToDelete = getContent(contentId);
        removeFollowersOfContent(contentToDelete);
        contentRepository.delete(contentToDelete);
    }


    public void removeFollowersOfContent(Content content){
        List<User> followers = userRepository.findAllByStarredContentsContaining(content);
        userRepository.saveAll(followers.stream().peek(user -> user.getStarredContents().remove(content)).collect(Collectors.toList()));
    }
}
