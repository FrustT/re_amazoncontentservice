package com.researchecosystems.contentserviceapi.service;

import com.researchecosystems.contentserviceapi.entity.Invoice;
import com.researchecosystems.contentserviceapi.entity.PricingSchedule;
import com.researchecosystems.contentserviceapi.entity.User;
import com.researchecosystems.contentserviceapi.entity.UserRole;
import com.researchecosystems.contentserviceapi.exception.BusinessException;
import com.researchecosystems.contentserviceapi.exception.ErrorCode;
import com.researchecosystems.contentserviceapi.model.response.invoice.InvoiceResponse;
import com.researchecosystems.contentserviceapi.repository.InvoiceRepository;
import com.researchecosystems.contentserviceapi.repository.PricingScheduleRepository;
import com.researchecosystems.contentserviceapi.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class InvoiceService {

    private final PricingScheduleRepository pricingScheduleRepository;
    private final InvoiceRepository invoiceRepository;
    private final UserService userService;
    private final UserRepository userRepository;

    public Page<InvoiceResponse> listInvoices(Pageable pageable) {
        return invoiceRepository.findAll(pageable)
                .map(InvoiceResponse::fromEntity);
    }
    public List<PricingSchedule> listPlans(){
        return pricingScheduleRepository.findAllByActive(true);
    }

    public PricingSchedule getPricingScheduleById(String id) {
        return pricingScheduleRepository.findByIdAndActive(id, true)
                .orElseThrow(() -> new RuntimeException("Pricing schedule not found or inactive"));
    }
    public List<Invoice> getInvoicesOfUser(String userId) {
        return invoiceRepository.findAllByUser(userService.getUser(userId));
    }
    public Invoice getLastUnpaidInvoiceOfUser(String userId) {
        List<Invoice> dbResult = invoiceRepository.findAllByUserAndPaid(userService.getUser(userId), false);
        if (dbResult.isEmpty()) {
            throw new BusinessException(ErrorCode.resource_missing, "No unpaid invoice found");
        }
        return dbResult.get(0);
    }
    public List<Invoice> getOverdueInvoices() {
        return invoiceRepository.findAllByPaidAndDueDateIsBefore(false, ZonedDateTime.now());
    }
    public List<Invoice> getInvoicesToActivate() {
        return invoiceRepository.findAllByActiveAndActivationDateBefore(false, ZonedDateTime.now());
    }
    public List<Invoice> createAPaymentScheduleForUser(String userId, String pricingScheduleId) {
        User user = userService.getUser(userId);
        PricingSchedule pricingSchedule = getPricingScheduleById(pricingScheduleId);
        List<Invoice> invoices = pricingSchedule.createInvoices();
        invoices.forEach(invoice -> invoice.setUser(user));
        invoiceRepository.saveAll(invoices);
        activateInvoices();
        return invoices;
    }
    public InvoiceResponse payInvoice(String invoiceId) {
        Invoice invoice = invoiceRepository.findById(invoiceId)
                .orElseThrow(() -> new BusinessException(ErrorCode.resource_missing, "Invoice not found"));
        invoice.setPaid(true);
        invoice.setActive(false);
        userService.grantContentPermission(invoice.getUser().getId());
        invoiceRepository.save(invoice);
        return InvoiceResponse.fromEntity(invoice);
    }
    public void activateInvoices(){
        List<Invoice> invoices = getInvoicesToActivate();
        invoices.forEach(invoice -> invoice.setActive(true));
        invoiceRepository.saveAll(invoices);
    }
    public void processOverdueInvoices(){
        List<Invoice> invoices = getOverdueInvoices();
        invoices.forEach(invoice -> {
            invoice.setActive(false);
            User user = invoice.getUser();
            userService.revokeContentPermission(user.getId());
            delayLaterInvoices(user.getId());
        });
        invoiceRepository.saveAll(invoices);
    }
    public void processInvoices(){
        processOverdueInvoices();
        activateInvoices();
    }
    public void delayLaterInvoices(String userId){
        List<Invoice> invoices = getInvoicesOfUser(userId);
        for (int i = 1; i < invoices.size(); i++) {
            Invoice invoice = invoices.get(i);
            invoice.setDueDate(null);
            invoice.setActivationDate(null);
        }


        invoiceRepository.saveAll(invoices);
    }
    public void subscribe(String userId, String pricingScheduleId){
        createAPaymentScheduleForUser(userId, pricingScheduleId); //PriceCheck will be done here
    }
}


