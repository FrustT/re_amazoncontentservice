package com.researchecosystems.contentserviceapi.service;

import com.researchecosystems.contentserviceapi.entity.User;
import com.researchecosystems.contentserviceapi.exception.BusinessException;
import com.researchecosystems.contentserviceapi.exception.ErrorCode;
import com.researchecosystems.contentserviceapi.model.request.auth.*;
import com.researchecosystems.contentserviceapi.model.request.auth.register.RegisterRequest;
import com.researchecosystems.contentserviceapi.model.response.LoginResponse;
import com.researchecosystems.contentserviceapi.repository.UserRepository;
import com.researchecosystems.contentserviceapi.security.JwtService;
import com.researchecosystems.contentserviceapi.service.client.EmailClient;
import com.researchecosystems.contentserviceapi.util.DateUtil;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AuthenticationService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final JwtService jwtService;
    private final EmailClient emailClient;

    public void register(RegisterRequest registerRequest) {
        if (userRepository.existsByEmail(registerRequest.getEmail())) {
            throw new BusinessException(ErrorCode.account_already_exists, "Account already exists");
        }

        String verificationCode = RandomStringUtils.randomAlphanumeric(24);
        ZonedDateTime verificationCodeExpirationDate = DateUtil.now().plusDays(1);

        User user = new User();
        user.setEmail(registerRequest.getEmail());
        user.setPasswordHash(passwordEncoder.encode(registerRequest.getPassword()));
        user.setVerificationCode(verificationCode);
        user.setVerificationCodeExpirationDate(verificationCodeExpirationDate);
        user.setVerified(false);
        userRepository.save(user);

        emailClient.sendVerificationEmail(user);
    }

    public LoginResponse login(LoginRequest loginRequest) {
        User user = userRepository.findFirstByEmail(loginRequest.getEmail())
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "User not found"));

        if (!passwordEncoder.matches(loginRequest.getPassword(), user.getPasswordHash())) {
            throw new BusinessException(ErrorCode.password_mismatch, "Wrong Password");
        }
        if(!user.isVerified()) {
            throw new BusinessException(ErrorCode.forbidden, "User is not verified");
        }

        return LoginResponse.builder()
                .id(user.getId())
                .token(jwtService.createToken(user.getId()))
                .userRole(user.getUserRole())
                .build();
    }

    public Optional<User> getAuthenticatedUser() {
        String principal = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal.equals("anonymousUser")) {
            return Optional.empty();
        }
        return userRepository.findById(principal);
    }

    public String sendVerificationEmail(String email) {
        System.out.println("AuhthenticationService.sendVerificationEmail method ");
        User user = userRepository.findByEmail(email);
        if(user == null) {
            throw new BusinessException(ErrorCode.account_missing, "User not found");
        }


        if (user.isVerified()) {
            throw new BusinessException(ErrorCode.forbidden, "User is already verified");
        }

        String verificationCode = RandomStringUtils.randomAlphanumeric(24);
        ZonedDateTime verificationCodeExpirationDate = DateUtil.now().plusDays(1);

        user.setVerificationCode(verificationCode);
        user.setVerificationCodeExpirationDate(verificationCodeExpirationDate);
        userRepository.save(user);
        emailClient.sendVerificationEmail(user);
        return getUrl("/auth/verify/" + email + "?code=" + user.getVerificationCode());
    }

    public void verify(String email, String verificationCode) {
        User user = userRepository.findFirstByEmail(email)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "User not found"));
        if(user.isVerified()) {
            throw new BusinessException(ErrorCode.forbidden, "User is already verified");
        }
        if (user.getVerificationCodeExpirationDate().isBefore(DateUtil.now())) {
            userRepository.save(clearVerificationCode(user));
            throw new BusinessException(ErrorCode.code_expired, "Verification code has expired");
        }
        if (!user.getVerificationCode().equals(verificationCode)) {
            throw new BusinessException(ErrorCode.code_mismatch, "Wrong verification code");
        }

        user.setVerified(true);
        userRepository.save(clearVerificationCode(user));
    }

    public String recoverPasswordRequest(String email) {
        User user = userRepository.findFirstByEmail(email)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "User not found"));

        if (!user.isVerified()) {
            throw new BusinessException(ErrorCode.conflict, "User is not verified");
        }
        String recoveryCode = RandomStringUtils.randomAlphanumeric(24);
        ZonedDateTime recoveryCodeExpiredDate = DateUtil.now().plusDays(1);

        user.setRecoveryCode(recoveryCode);
        user.setRecoveryCodeExpirationDate(recoveryCodeExpiredDate);
        userRepository.save(user);

        emailClient.sendRecoveryEmail(user);
        return getUrl("/auth/recover/" + email + "?code=" + user.getRecoveryCode());
    }

    public void resetPassword(String email,String recoveryCode, PasswordChangeRequest passwordChangeRequest) {
        User user = userRepository.findFirstByEmail(email)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "User not found"));
        if (!user.isVerified()) {
            throw new BusinessException(ErrorCode.conflict, "User is not verified");
        }
        if (!user.getRecoveryCode().equals(recoveryCode)) {
            throw new BusinessException(ErrorCode.code_mismatch, "Wrong verification code");
        }
        if(!passwordChangeRequest.getNewPassword().equals(passwordChangeRequest.getNewPasswordAgain())) {
            throw new BusinessException(ErrorCode.password_mismatch, "Password mismatch");
        }
        if(user.getRecoveryCodeExpirationDate().isBefore(DateUtil.now())) {
            System.out.println("AuhthenticationService.resetPassword method recoveryCodeExpirationDate.isBefore(DateUtil.now())");
            userRepository.save(clearRecoveryCode(user));
            throw new BusinessException(ErrorCode.code_expired, "Recovery code has expired");
        }

        user.setPasswordHash(passwordEncoder.encode(passwordChangeRequest.getNewPassword()));
        userRepository.save(clearRecoveryCode(user));
    }
    private String getUrl(String uri){
        String address = "http://localhost:8080"; //TODO: get address from config
        return address + uri;
    }

    private User clearRecoveryCode(User user){
        user.setRecoveryCode(null);
        user.setRecoveryCodeExpirationDate(null);
        return user;
    }
    private User clearVerificationCode(User user){
        user.setVerificationCode(null);
        user.setVerificationCodeExpirationDate(null);
        return user;
    }
}
