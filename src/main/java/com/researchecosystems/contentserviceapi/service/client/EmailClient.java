package com.researchecosystems.contentserviceapi.service.client;

import com.researchecosystems.contentserviceapi.entity.User;
import lombok.Getter;
import org.springframework.stereotype.Service;

@Service
@Getter
public class EmailClient {


    private boolean verificationEmailSent = false;

    private boolean recoveryEmailSent = false;

    public void sendVerificationEmail(User user) {
        this.verificationEmailSent = true;
    }

    public void sendRecoveryEmail(User user) {
        this.recoveryEmailSent = true;
    }

}

